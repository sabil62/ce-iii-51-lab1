import random
from linear_search import linear_search
from time import time
import matplotlib.pyplot as plt
dataToPlot = {
    "index": [],
    "duration": {
        "best": [],
        "worst": [],
    }
}

for i in range(1000, 100001, 5000):
    data = random.sample(range(100000), i)
    dataToPlot['index'].append(i)

    # for best case
    start = time()
    linear_search(data, data[0])
    duration = time() - start
    dataToPlot['duration']["best"].append(duration)

    # for worst case
    start = time()
    linear_search(data, -10000)
    duration = time() - start
    dataToPlot['duration']["worst"].append(duration)

plt.plot(dataToPlot["index"], dataToPlot["duration"]
         ["best"], c="green", label="Best Case")
plt.plot(dataToPlot["index"], dataToPlot["duration"]
         ["worst"], c="red", label="Worst Case")
plt.legend()
plt.show()