import unittest
from linear_search import linear_search

class TestSearch(unittest.TestCase):

    def test_search(self):
        data = [21,34,75,5,8,77]

        self.assertEqual(linear_search(data, 75), 2)
        self.assertEqual(linear_search(data, 100),  -1)


    def test_searchChar(self):
        data = ['r', 'a', 'n', 'd', 'o','m']
        self.assertEqual(linear_search(data, 'm'), 5)

    
if __name__ == "__main__":
    unittest.main()
