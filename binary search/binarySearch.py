def binary_search_func(array, minimum, maximum, target):
    if maximum>=minimum:
        #middle element
        middle= (maximum + minimum) //2           #the // is the floor division operator

        if array[middle] == target:
            return middle
        elif array[middle] > target:
            return binary_search_func(array, minimum, middle - 1, target)
        else:
            return binary_search_func(array, middle +1 , maximum, target)

    else:
        return -1
    