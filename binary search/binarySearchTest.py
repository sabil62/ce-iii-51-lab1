import unittest
from binarySearch import binary_search_func

class TestBinaryFunc(unittest.TestCase):

    #For numbers
    def test_search(self):
        test_array=[12,24,36,48,60,72,84,96]
        min=0
        max=len(test_array)-1
        target = 72
        self.assertEqual(binary_search_func(test_array, min ,max, target),5)        #here 72(target) is in 5th place so this should be true, if we put 6 then errro
        #this is to check error explained below
        self.assertEqual(binary_search_func(test_array, min, max, 26545 ),-1)        #here 26565 is not in array so -1 is error place

    #For characters
    def test_search_character(self):
        array_charac=['D','i','a','m','o','n','d']
        self.assertEqual(binary_search_func(array_charac,0,len(array_charac) - 1,'n'),5)    #n is in 5th place,     if 6 then error

    
if __name__ == "__main__":
    unittest.main()
