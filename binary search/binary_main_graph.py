from binarySearch import binary_search_func
import matplotlib.pyplot as plt
import random
from time import time

dataToPlot = {
    "index": [],
    "duration": {
        "best": [],
        "worst": [],
    }
}
for i in range(1000, 100001, 1000):         #this is range(start,stop,step)
    data = random.sample(range(100000), i)
   #for index range
    dataToPlot['index'].append(i)

    # for best case
    middle = data[(i-1)//2]

    start = time()
    binary_search_func(data, 0, i-1, middle) #the data is exactly at middle
    duration = time() - start
   

    dataToPlot["duration"]["best"].append(duration)

    # for worst case
    start = time()
    binary_search_func(data, 0, i-1, -5) #-5 isnt defined in array so binary wont find any
    duration = time() - start

 

    dataToPlot["duration"]["worst"].append(duration)


#best case plot
plt.plot(dataToPlot["index"], dataToPlot["duration"]["best"], c="green", label="Best Case")
#worst cast plot
plt.plot(dataToPlot["index"], dataToPlot["duration"]["worst"], c="red", label="Worst Case")
plt.legend()
plt.show()